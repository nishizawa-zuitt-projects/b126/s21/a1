/* Activity:
	Design a data model using the same syntax we use in d1 for your upcoming capstone 2
	project, which will be an ecommerce website.

- Specify at least the following collections:
- Users
- Products

Make sure all possible and necessary relationships between data is properly established
Once done, we will present our work to the class.
*/

/*
Project (EC site): EC site of a magic shop

Collections:
	- Collection of Users
		Specific Users
			Fields
				userId - data type: MongoDB ID
				userName - data type: String
				password - data type: String
				userEmail - data type: String
				shippingAddress - data type: String

	- Collection of Products
		Specific Products
			Fields
				productId = MongoDB ID
				productName = String
				productDescription = String
				releasedDate = number
				productCategory = string
				productPrice = number

	- Collections of Orders
		Orders
			Fields
				productID = MongoDB ID
				userID = MongoDB ID
				shippingAddress = String
				quantity = number
				totalAmount = number
				date = number
*/




